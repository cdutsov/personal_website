#!/bin/bash

filename="$1"

post=$(cat "$filename")
images=$(grep "<img" "$filename")
header_pos=$(grep -n -- "---" $filename | tail -1 | cut -d ":" -f 1)
header_pos_split=$(($header_pos - 1))

yaml_add="images:"
while read -r image; do
    src=$(echo $image | sed 's/<[a-zA-Z ="{}.]*\//\//g' | sed 's/".*$//g')
    alt=$(echo $image | sed 's/^.*alt="//g' | sed 's/".*$//g')
    yaml_add="$yaml_add"$'\n  - source: '\"$src\"$'\n    caption: '\"$alt\"$'\n    title: '\"$alt\"
done < <(grep "<img" "$filename")

head -"$header_pos_split" "$filename"
echo "$yaml_add"
tail +"$header_pos" "$filename"
