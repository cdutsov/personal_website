---
title: "Online database for TDCR measurements"
excerpt: "Store and organize large amount of measurements<br/><img src='/images/tdcr_online.png' style='width: 30vw; min-width: 200px;' /><br>"
collection: software
---

Online database for TDCR measurements designed to store and organize large
amount of data. It is written in Python with the Flask framework and uses MySQL
as a backend. The main advantage of this tool is that it can combine data from
multiple computers and organize it by date, measured sample, radionuclide,
liquid scintillation coctail and other critera. The website currently resides at
[tdcr.xyz](https://tdcr.xyz), but you need an acount to use it. Please contact
me if you would like to use it.

Source code and manual can be found at: [GitLab](https:/gitlab.com/cdutsov/tdcr_analysis_v2)
