---
title: "Command line sofware to analyze CAEN digitizer data"
excerpt: "Fast code to analyze 100+ GB of digitizer files<br/><img src='/images/cdt_logic.png' style='width: 30vw; min-width: 200px;' /><br>"
collection: software
---

Command line interface software with the ability to calculate double and triple coincidences from
CAEN digitizer data. It works with all types of output data (.csv and .bin) and with all recent versions
of the CoMPASS software from CAEN. The code can also construct the time interval distributions between
pairs of channels and can apply algorithms for evaluation of accidental coincidences. It allows for
very accurate time interval distributions without concern of the counting rate of the studied source.

The code is written in Rust and is very fast. The only limiting factor for the speed of execution is
the speed of the storage device.

Source code and manual at: [GitLab](https:/gitlab.com/cdutsov/cdt_logic)
