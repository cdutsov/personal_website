---
title: "Graphical TDCR data analysis software"
excerpt: "Software to analyze large amounts of TDCR measurements suited for radionuclide metrology<br/><img src='/images/tdcr_offline_photo.png' style='width: 30vw; min-width: 200px;' /><br>"
collection: software
---

Graphical software to analyze large amount of TDCR measurements with the nanoTDCR acquisition device.
The software has the ability to calculate the activity of pure-beta emitters using the TDCR18 code.
It has a database of the decay parameters of commonly used radionuclides and can apply corrections for
decay during measurement and calculate the counting rates and activity for a reference time.
It is also very useful for studies of the Birks parameter of liquid scinitllators, where a large amount of
data is generated.

Source code and manual at: [GitLab](https:/gitlab.com/cdutsov/tdcr_analysis_offline)
