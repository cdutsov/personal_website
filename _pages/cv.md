---
layout: archive
title: "Resume"
permalink: /cv/
author_profile: true
redirect_from:
  - /resume
---

{% include base_path %}

{% include mathjax.js %}

<!-- <iframe src="../files/resume_chavdar_dutsov.pdf" width="100%" height="500" frameborder="no" border="0" marginwidth="0" marginheight="0"></iframe> -->

Download the pdf [here](../files/resume_chavdar_dutsov.pdf){:target="\_blank"}

## Education

- PhD in Nuclear Physics, [Sofia University "St. Kliment Ohridski"](https://www.uni-sofia.bg/index.php/eng) 2018 - 2021
  - Thesis: Primary standardization of radioactivity using the TDCR method in Liquid Scintillation Counting
  - [download here](../files/phd_thesis.pdf){:target="\_blank"}
- MS in Medical Physics from [Sofia University "St. Kliment Ohridski"](https://www.uni-sofia.bg/index.php/eng) 2018 with excellent mark
  - Thesis: _Study on the applications of the TDCR method_
  - [download here](../files/master_thesis.pdf){:target="\_blank"}
- BS in Medical Physics from [Sofia University “St. Kliment Ohridski”](https://www.uni-sofia.bg/index.php/eng) 2016 with excellent mark
  - Thesis: _Determination of diffusion length and solubility of <sup>222</sup>Rn in plastic scintillators EJ-212 and BC-400_
  - [download here (in Bulgarian)](../files/bachelor_thesis.pdf){:target="\_blank"}
- Graduation from [“National High School of Mathematics and Science” Akad. L. Chakalov](https://npmg.org)
  - Class of Physics and Astronomy

## Work experience

- Spring 2022 - present: Postdoctoral Fellow

  - [Paul Scherrer Institut](https://psi.ch){:target="\_blank"}
  - Research on the systematic effects in the search for the electric dipole moment of the muon

- June 2019 – December 2019 & September 2020 - January 2021: Guest scientist

  - [Laboratoire National Henry Becqerel](https://lnhb.fr){:target="\_blank"}
  - Research on radionuclide metrology using liquid scinitillation counting
  - Research on fast timing of PMT signals in low-light scenario

- 2018: Physicist - [MetroRadon](http://metroradon.eu/) - project title “Metrology for radon monitoring“

- Spring 2018 – Fall 2021: Full time physicist

  - Sofia University „St. Kliment Ohridski“, Department of Atomic Physics
  - Work at the Metrology of Ionizing Radiation Laboratory
  - Duties: metrological measurements and studies, detector design and development, data analysis

- 2017: Physicist - [Polyrad](https://www.researchgate.net/project/POLYRAD)
  - project title: _“Rеsearch of properties of novel high-tech polymer materials for development of new radon measurement techniques“_

## Skills

- Programming languages
  - Python
    - example code: [gitlab.com/cdutsov/tdcr_analysis_offline](https://gitlab.com/cdutsov/tdcr_analysis_offline/-/tree/master/app)
  - C, C++
    - example code:
    - experience with Atmel and Microchip microcontrollers, parallel computing with MPI, Arduino
  - [Rust](https://www.rust-lang.org/)
  - FPGA programming via VHDL
    - programmed a Time-to-Amplitude Converter with ~20 ps timing resolution
  - Web development HTML+CSS+JavaScript, Python back-end
    - example code:
      [online database for management of experimental data](https://gitlab.com/cdutsov/tdcr_analysis-v2/-/tree/master/app)
  - Java (Android platform)
    - published several small applications for Android
- Linux
  - experience with Linux administration
  - good knowledge of tools such as: [Vim](https://www.vim.org/), git, [i3](https://i3wm.org/), systemd and the GNU
    core utilities

## Awards from competitions

1. Bronze Medal, [International Olympiad on Astronomy and Astrophysics](https://en.wikipedia.org/wiki/International_Olympiad_on_Astronomy_and_Astrophysics), Rio de Janeiro, Brazil, 2012
2. Silver Medal, [International Astronomy Olympiad](https://en.wikipedia.org/wiki/International_Astronomy_Olympiad), Trieste, Italy 2008
3. First Place, Bulgarian National Physics Competition, Shumen 2012
4. Participation Award, [International Astronomy Olympiad](https://en.wikipedia.org/wiki/International_Astronomy_Olympiad), Shanhai, China 2009
5. First Place, National Olympiad on Computer Mathematics, Bulgaria, 2015
6. Award in the name of G. Nadzhakov from Eureka Foundation, 2015
7. Award for exceptional achievements in the field of Physics and Astronomy from the National High School of Mathematics and Science “Akad. L. Chakalov” 2012
8. Siver Medal, National Olympiad on Mathematics, 2015
9. Bronze Medal, National Olympiad on Mathematics 2016
10. Third Place, National Olympiad on Astronomy 2010
11. First Place in the National Contest “Space – present and future of mankind” for the creation of an online astronomy encyclopedia in bulgarian. 2007
12. Third Place, National Robotics Competition, Technical University Sofia, 2015
13. First Place, SoftUni Tech Fest – category Robotics, 2017

## Certificates

- English language certificate – IELTS; score 7,5/9
- English language certificate – TOEFL; score 112/120
- SAT Physics 800/800
- SAT Mathematics II 750/800
- SAT Mathematics I 760/800
- SAT Critical Reading - 620/800
- SAT Writing – 650/800
- French level A2

## Selected Publications

  <ul>{% for post in site.publications reversed%}
    {% include archive-single-cv.html %}
  {% endfor %}</ul>
