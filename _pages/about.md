---
permalink: /
title: ""
excerpt: "About me"
author_profile: true
redirect_from:
  - /about/
  - /about.html
---

# About me

I am a PostDoctoral Fellow at [Paul Scherrer Institute](https://psi.ch) at the Laboratory for Particle Physics. I am working on the μEDM experiment which aims for the highest precision measurement of the electric dipole moment of the muon. I enjoy working on GNU/Linux and I am a free open source software enthusiast. I like programming and building electronic circuits with the idea that if something can be automated, then it should be automated.

Except physics and computers, my interests include mountain biking, hiking and skiing. I have recently started making digital drawings. Feel free to explore my [virtual gallery](https://gallery.physica.dev).
