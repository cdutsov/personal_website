---
layout: archive
title: Publications
permalink: /publications/
author_profile: true
---

{% include base_path %}

{% include mathjax.js %}

A full list of my publications in pdf format can be downloaded from [here](../files/list_of_pubs_Chavdar_Dutsov.pdf){:target="\_blank"}

## Selected publications on radionuclide metrology

{% for post in site.publications reversed %}
{% if post.section == 'metrology' %}
{% include archive-single.html %}
{% endif %}
{% endfor %}

## Selected publications on Radon measurements

{% for post in site.publications reversed %}
{% if post.section == 'radon' %}
{% include archive-single.html %}
{% endif %}
{% endfor %}
