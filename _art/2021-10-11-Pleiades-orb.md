---
layout: single
title: "The Pleiades"
date: 2021-10-11
header:
  teaser: "/drawings/pleiades_orb_teaser.webp"
  art: "/drawings/pleiades_orb_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/pleiades_orb_teaser.webp"
    caption: "Drawing of the Pleiades visible in a drop of water fallen on a leaf"
    title: "Drawing of the Pleiades visible in a drop of water fallen on a leaf"
---

Drawing of the Pleiades visible in a drop of water fallen on a leaf.

<a href="{{ site.url }}/images/drawings/pleiades_orb.png" download>
<img src="{{ site.url }}/images/drawings/pleiades_orb_teaser.webp" alt="Drawing of the Pleiades visible in a drop of water fallen on a leaf"> </a>
Click on the image to download in full size.
