---
layout: single
title: "Drawing of Neptune seen from Triton"
date: 2021-09-09
header:
  teaser: "/drawings/neptune_variant_teaser.webp"
  art: "/drawings/neptune_variant_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/neptune_teaser.webp"
    caption: "Drawing of Neptune as seen from Triton"
    title: "Drawing of Neptune as seen from Triton"
  - source: "/images/drawings/neptune_variant_teaser.webp"
    caption: "Drawing of Neptune as seen from Triton (orange version)"
    title: "Drawing of Neptune as seen from Triton (orange version)"
---

A drawing of Neptune as seen from its moon Triton. Some natural phenomena specific to Triton are visible - liquid nitrogen geysers and lakes. Triton has a very thin atmosphere, but it should form nitrogen mist near the surface. Triton's surface is new as it has tectonic activity, but there might be a few craters here and there, as the one seen on the drawing.

The drawing was made on the Galaxy Tab S6 Lite with the Sketchbook app.

<a href="{{ site.url }}/images/drawings/neptune.png" download>
<img src="{{ site.url }}/images/drawings/neptune_teaser.webp" alt="Drawing of Neptune as seen from Triton"> </a>


PS: After discussing with a friend, it seems that the atmosphere of Triton could be such that it gives orange tint to sunlight, especially at sunrise or sunset. So I decided to make another variant of the drawing with more orange colors. I think I like it more that way.

What can also be seen on the picture are Uranus, Saturn and Jupiter, shining near the sun.

<a href="{{ site.url }}/images/drawings/neptune_variant.png" download>
<img src="{{ site.url }}/images/drawings/neptune_variant_teaser.webp" alt="Drawing of Neptune as seen from Triton (orange version)"> </a>

Click on the image to download in full size.
