---
layout: single
title: "The Red Giant"
date: 2021-09-13
header:
  teaser: "/drawings/red_giant_flare_sb_teaser.webp"
  art: "/drawings/red_giant_flare_sb_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/red_giant_flare_sb_teaser.webp"
    caption: "Drawing of a red gas giant planet"
    title: "Drawing of a red gas giant planet"
  - source: "/images/drawings/red_giant_teaser.webp"
    caption: "Drawing of a red gas giant planet"
    title: "Drawing of a red gas giant planet"
---

A view of a red gas giant planet near its ring.

<a href="{{ site.url }}/images/drawings/red_giant_flare_sb.png" download>
<img src="{{ site.url }}/images/drawings/red_giant_flare_sb_teaser.webp" alt="Drawing of a red gas giant planet"> </a>

I made also a variant without the lens flare.

<a href="{{ site.url }}/images/drawings/red_giant.png" download>
<img src="{{ site.url }}/images/drawings/red_giant_teaser.webp" alt="Drawing of a red gas giant planet"> </a>

Click on the image to download in full size.
