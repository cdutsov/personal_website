---
layout: single
title: "Messier Thirty-One"
date: 2021-10-13
header:
  teaser: "/drawings/m31_teaser.webp"
  art: "/drawings/m31_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/m31_teaser.webp"
    caption: "Drawing of M31 - Andromeda Galaxy"
    title: "Drawing of M31 - Andromeda Galaxy"
---

Drawing of the Andromeda galaxy. It was supposed to be part of a more complex
picture, but I liked it the way it is.

<a href="{{ site.url }}/images/drawings/m31.png" download>
<img src="{{ site.url }}/images/drawings/m31_teaser.webp" alt="Drawing of M31 - Andromeda Galaxy"> </a>
Click on the image to download in full size.
