---
layout: single
title: "The big mantle of the white dwarf"
date: 2021-11-05
header:
  teaser: "/drawings/ring_nebula_teaser.webp"
  art: "/drawings/ring_nebula_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/ring_nebula_teaser.webp"
    caption: "Drawing of M57 The ring nebula"
    title: "Drawing of M57 The ring nebula"
---

Drawing of the planetary nebula Messier 57 (Ring Nebula).

<a href="{{ site.url }}/images/drawings/ring_nebula.png" download>
<img src="{{ site.url }}/images/drawings/ring_nebula_teaser.webp" alt="Drawing of M57 The ring nebula"> </a>
Click on the image to download in full size.
