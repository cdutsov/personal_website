---
layout: single
title: "The Buzludzha monument flying away"
date: 2021-09-09
header:
  teaser: "/drawings/buzludzha_teaser.webp"
  art: "/drawings/buzludzha_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/buzludzha_teaser.webp"
    caption: "Drawing of the Buzludzha monument flying away"
    title: "Drawing of the Buzludzha monument flying away"
---

A drawing of the [Buzludzha monument](https://en.wikipedia.org/wiki/Buzludzha_monument) flying away into space. The monument always reminded me of a sci-fi spaceship so I decided to draw it that way.

<a href="{{ site.url }}/images/drawings/buzludzha.png" download>
<img src="{{ site.url }}/images/drawings/buzludzha_teaser.webp" alt="Drawing of the Buzludzha monument flying away"> </a>
Click on the image to download in full size.
