---
layout: single
title: "Drawing of Saturn"
date: 2021-08-04
header:
  teaser: "/drawings/saturn_variant_teaser.webp"
  art: "/drawings/saturn_variant_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/saturn_teaser.webp"
    caption: "Drawing of Saturn"
    title: "Drawing of Saturn"
  - source: "/images/drawings/saturn_variant_teaser.webp"
    caption: "Drawing of Saturn with stars"
    title: "Drawing of Saturn with stars"
---

A drawing of Saturn and his majestic rings.
The drawing was made on the Galaxy Tab S6 Lite with the Sketchbook app.

<a href="{{ site.url }}/images/drawings/saturn.png" download>
<img src="{{ site.url }}/images/drawings/saturn_teaser.webp" alt="Drawing of Saturn"> </a>


I made a variant of the same image, but with added stars. Not photorealistic, but more pretty I think.

<a href="{{ site.url }}/images/drawings/saturn_variant.png" download>
<img src="{{ site.url }}/images/drawings/saturn_variant_teaser.webp" alt="Drawing of Saturn with stars"> </a>
Click on the image to download in full size.
