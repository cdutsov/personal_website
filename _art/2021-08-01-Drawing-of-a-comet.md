---
layout: single
title: "My first drawing on a tablet"
date: 2021-08-01
header:
  teaser: "/drawings/comet_teaser.webp"
  art: "/drawings/comet_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/comet_teaser.webp"
    caption: "Drawing of a comet"
    title: "Drawing of a comet"
---

A drawing of a comet. This was my first drawing on a tablet. It's not very good, but it got me excited for digital drawings.
The drawing was made on a Samsung Galaxy S6 Lite tabled with the S-pen.

<a href="{{ site.url }}/images/drawings/comet.png" download>
<img src="{{ site.url }}/images/drawings/comet_teaser.webp" alt="Drawing of a comet"> </a>
Click on the image to download in full size.
