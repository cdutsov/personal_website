---
layout: single
title: "Drawing of a sunny beach"
date: 2021-09-19
header:
  teaser: "/drawings/palms_teaser.webp"
  art: "/drawings/palms_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/palms_teaser.webp"
    caption: "Drawing of palms on the beach during sunrise"
    title: "Drawing of palms on the beach during sunrise"
---

Drawing of palms on the beach during sunrise. 
It was heavily inspired by a drawing I saw on the PenUp app by [this author](https://anastelfy.gumroad.com/?sort=page_layout).

<a href="{{ site.url }}/images/drawings/palms.png" download>
<img src="{{ site.url }}/images/drawings/palms_teaser.webp" alt="Drawing of palms on the beach during sunrise"> </a>

Click on the image to download in full size.
