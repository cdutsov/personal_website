---
layout: single
title: "Fishing on the lake"
date: 2021-09-01
header:
  teaser: "/drawings/fishing_on_the_lake_teaser.webp"
  art: "/drawings/fishing_on_the_lake_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/fishing_on_the_lake_teaser.webp"
    caption: "Fishing on the lake"
    title: "Fishing on the lake"
---

The drawing was made on the Galaxy Tab S6 Lite with the Sketchbook app.

<a href="{{ site.url }}/images/drawings/fishing_on_the_lake.png" download>
<img src="{{ site.url }}/images/drawings/fishing_on_the_lake_teaser.webp" alt="Fishing on the lake"> </a>
Click on the image to download in full size.
