---
layout: single
title: "Drawing of the Northern Lights"
date: 2021-08-03
header:
  teaser: "/drawings/northern_lights_teaser.webp"
  art: "/drawings/northern_lights_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/northern_lights_teaser.webp"
    caption: "Drawing of the Northern Lights in Bob Ross style"
    title: "Drawing of the Northern Lights in Bob Ross style"
---

A drawing of the Northern Lights with mountains and a lake.
The drawing was made on a Samsung Galaxy S6 Lite tabled with the S-pen.
It's a very nice device for drawing!

<a href="{{ site.url }}/images/drawings/northern_lights.png" download>
<img src="{{ site.url }}/images/drawings/northern_lights_teaser.webp" alt="Drawing of the Northern Lights in Bob Ross style"> </a>
Click on the image to download in full size.
