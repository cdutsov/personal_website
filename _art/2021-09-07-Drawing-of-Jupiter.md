---
layout: single
title: "Drawing of Jupiter and Io"
date: 2021-09-07
header:
  teaser: "/drawings/jupiter_teaser.webp"
  art: "/drawings/jupiter_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/jupiter_teaser.webp"
    caption: "Drawing of Jupiter and Io"
    title: "Drawing of Jupiter and Io"
---

A drawing of Jupiter and its moon Io seen when the Juno spacecraft approaches them. Europa can be seen in the distance, casting its shadow over the gas giant.
The drawing was made on the Galaxy Tab S6 Lite with the Sketchbook app.

<a href="{{ site.url }}/images/drawings/jupiter.png" download>
<img src="{{ site.url }}/images/drawings/jupiter_teaser.webp" alt="Drawing of Jupiter and Io"> </a>
Click on the image to download in full size.
