---
layout: single
title: "Drawing of a hand holding a shiny crystal"
date: 2021-08-09
header:
  teaser: "/drawings/hand_holding_crystal_teaser.webp"
  art: "/drawings/hand_holding_crystal_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/hand_holding_crystal_teaser.webp"
    caption: "Drawing of a hand holding a shiny crystal"
    title: "Drawing of a hand holding a shiny crystal"
  - source: "/images/drawings/hand_holding_crystal_variant_teaser.webp"
    caption: "Drawing of a hand and a levitating shiny crystal"
    title: "Drawing of a hand and a levitating shiny crystal"
  - source: "/images/drawings/hand_teaser.webp"
    caption: "Drawing of a hand and a levitating shiny crystal"
    title: "Drawing of a hand and a levitating shiny crystal"
---

A drawing of a hand holding a blue kyber crystal used to construct a lightsaber.
The drawing was made on the Galaxy Tab S6 Lite with the Sketchbook app.

<a href="{{ site.url }}/images/drawings/hand_holding_crystal.png" download>
<img src="{{ site.url }}/images/drawings/hand_holding_crystal_teaser.webp" alt="Drawing of a hand holding a shiny crystal"> </a>


There is a variant of the drawing with the crystal levitating.

<a href="{{ site.url }}/images/drawings/hand_holding_crystal_variant.png" download>
<img src="{{ site.url }}/images/drawings/hand_holding_crystal_variant_teaser.webp" alt="Drawing of a hand and a levitating shiny crystal"> </a>


There is also only the layer with the hand and lights and shadows.

<a href="{{ site.url }}/images/drawings/hand.png" download>
<img src="{{ site.url }}/images/drawings/hand_teaser.webp" alt="Drawing of a hand and a levitating shiny crystal"> </a>

I also made a mix between this pic and the Pleiades one.

<a href="{{ site.url }}/images/drawings/hand_holding_crystal_variant2.png" download>
<img src="{{ site.url }}/images/drawings/hand_holding_crystal_variant2_teaser.webp" alt="Drawing of a hand and the Pleiades"> </a>

Click on the image to download in full size.
