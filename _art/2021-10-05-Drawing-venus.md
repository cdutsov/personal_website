---
layout: single
title: "Mariner 10 passing near Venus"
date: 2021-10-05
header:
  teaser: "/drawings/venus_mariner_teaser.webp"
  art: "/drawings/venus_mariner_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/venus_mariner_teaser.webp"
    caption: "Drawing of the Northern Lights in Bob Ross style"
    title: "Drawing of the Northern Lights in Bob Ross style"
---

A drawing of the Mariner 10 spacecraft passing near Venus on its long journey to Mercury.

<a href="{{ site.url }}/images/drawings/venus_mariner.png" download>
<img src="{{ site.url }}/images/drawings/venus_mariner_teaser.webp" alt="Drawing of the Mariner 10 spacecraft passing near Venus"> </a>
Click on the image to download in full size.
