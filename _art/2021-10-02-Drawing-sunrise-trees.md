---
layout: single
title: "A spotted sun rising behind the hills"
date: 2021-10-02
header:
  teaser: "/drawings/sunrise_trees_teaser.webp"
  art: "/drawings/sunrise_trees_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/sunrise_trees_teaser.webp"
    caption: "A spotted sun rising behind the hills"
    title: "A spotted sun rising behind the hills"
---

Digital drawing of the Sun rising behind a hill.

This is a reimagined[^1] version of an "Astronomy Picture of the Day" (APOD) [photograph](https://apod.nasa.gov/apod/ap210921.html).

<a href="{{ site.url }}/images/drawings/sunrise_trees.png" download>
<img src="{{ site.url }}/images/drawings/sunrise_trees_teaser.webp" alt="Drawing of the Northern Lights in Bob Ross style"> </a>
Click on the image to download in full size.

[^1]: Reimagined as in how Holywood "reimagines" the same movies every 10 years. Credit to [Dr. Ivo Iliev](https://theorycorner.com/) for the term.
