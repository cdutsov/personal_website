---
layout: single
title: "Drawing of cave paintings"
date: 2021-08-07
header:
  teaser: "/drawings/cave_paintings_teaser.webp"
  art: "/drawings/cave_paintings_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/cave_paintings_teaser.webp"
    caption: "Drawing of cave paintings"
    title: "Drawing of cave paintings"
---

A drawing of the Altamira cave paintings lit by the fire.

<a href="{{ site.url }}/images/drawings/cave_paintings.png" download>
<img src="{{ site.url }}/images/drawings/cave_paintings_teaser.webp" alt="Drawing of cave paintings"> </a>
Click on the image to download in full size.
