---
layout: single
title: "Drawing of the Eiffel Tower"
date: 2021-08-05
header:
  teaser: "/drawings/Eiffel_tower_rain_teaser.webp"
  art: "/drawings/Eiffel_tower_rain_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/Eiffel_tower_rain_teaser.webp"
    caption: "Drawing of the Eiffel Tower reflected in a puddle"
    title: "Drawing of the Eiffel Tower reflected in a puddle"
---

Drawing of a scene in Paris where after an autumn rain the Eiffel tower is reflected in a puddle. The drawing was made on the Galaxy Tab S6 Lite with the Sketchbook app.

<a href="{{ site.url }}/images/drawings/Eiffel_tower_rain.png" download>
<img src="{{ site.url }}/images/drawings/Eiffel_tower_rain_teaser.webp" alt="Drawing of the Eiffel Tower reflected in a puddle"> </a>
Click on the image to download in full size.
