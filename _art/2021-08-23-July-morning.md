---
layout: single
title: "July Morning"
date: 2021-08-23
header:
  teaser: "/drawings/july_morning_teaser.webp"
  art: "/drawings/july_morning_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/july_morning_teaser.webp"
    caption: "July Morning"
    title: "July Morning"
---

A drawing of a fantastical July morning at the Black Sea.
The drawing was made on the Galaxy Tab S6 Lite with the Sketchbook app.
It was heavily inspired by a drawing I saw on the PenUp app by [this author](https://anastelfy.gumroad.com/?sort=page_layout).

<a href="{{ site.url }}/images/drawings/july_morning.png" download>
<img src="{{ site.url }}/images/drawings/july_morning_teaser.webp" alt="July Morning"> </a>
Click on the image to download in full size.
