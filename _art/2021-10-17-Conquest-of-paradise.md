---
layout: single
title: "Conquest of Paradise"
date: 2021-10-17
header:
  teaser: "/drawings/conquest_of_paradise_teaser.webp"
  art: "/drawings/conquest_of_paradise_teaser.webp"
categories:
  - drawings
collection: art
images:
  - source: "/images/drawings/conquest_of_paradise_teaser.webp"
    caption: "A ship sailing towards an unknown land"
    title: "A shipt sailing towards an unknown land"
---

A ship sailing towards an unknown land - his path illuminated by a distant galaxy.

<a href="{{ site.url }}/images/drawings/conquest_of_paradise.png" download>
<img src="{{ site.url }}/images/drawings/conquest_of_paradise_teaser.webp" alt="A ship sailing towards an unknown land"> </a>
Click on the image to download in full size.
