---
title: "Adding fancy drop caps in Latex documents"
header:
    teaser: "posts/fancy-lettrine.png"
date: 2021-06-04
permalink: /posts/2021/06/fancy-drop-caps-Latex.html
tags:
  - Latex
  - drop caps
---

Adding fancy drop caps in your LaTeX documents, like the one shown above, is
quite easy! You only need to use the package [lettrine](https://ctan.org/pkg/lettrine). To do that, simply add this to your preamble:

```latex
\usepackage{lettrine}
```

Then, every sentence that you want to start with a drop cap you can do this:

```latex
\lettrine{M}{y} fancy drop cap!
```

Voilà! Well, honestly, this will probably not be fancy enough for you. You can
always change the font however. There are many fonts that can be found in the
[tug.org font
catalogue](https://tug.org/FontCatalogue/otherfonts.html#initials). I prefer the
**Zallman Caps**, but, if you want to go full fancy, you can try **Goudy
Initialen**. A full list of the fonts available in the TeXLive distribution and
how to use them can be found in the __cfr-initials__ [package
documentation](https://mirrors.nxthost.com/ctan/macros/latex/contrib/cfr-initials/cfr-initials.pdf).
To use Goudy, for example, it is quite simple. Just add this to your preamble:

```latex
\usepackage{GoudyIn}
\renewcommand{\LettrineFontHook}{\GoudyInfamily{}}
```

Using the Zallman Caps is a bit harder but you can do it like so:

```latex
\begin{filecontents}{Zallman.sty}
	\NeedsTeXFormat{LaTeX2e}
	\ProvidesPackage{Zallman}[2007/11/24 v1.0 Zallman CFR]

	\input Zallman.fd

	\DeclareRobustCommand{\Zallmanfamily}{
		\fontencoding{U}%
		\fontseries{xl}%
		\fontshape{n}%
		\fontfamily{Zallman}%
		\selectfont}
	\DeclareTextFontCommand{\zall}{\Zallmanfamily}

	\endinput
	%% end Zallman.sty
\end{filecontents}
\usepackage{Zallman}
\renewcommand{\LettrineFontHook}{\color{Maroon}\Zallmanfamily}
```

By the way, you can change the color of the caps with `\color{some_color}`, like
in the example above.
![Fancy drop cap at the begining of the chapter](/images/posts/fancy-lettrine.png){:width="90%" .center-image}

Thats it and enjoy your fancy new capitals!
