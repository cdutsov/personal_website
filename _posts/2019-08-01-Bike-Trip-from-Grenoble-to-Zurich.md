---
title: "A Bike Trip from Grenoble to Zürich"
date: 2019-08-01
permalink: /posts/2019/08/trip-grenoble-zurich.html
tags:
  - trips
---

This is a short story with some pictures of my trip from Grenoble to Zürich,
testing my new bike for a few days.

## Day One

My trip actually started from Paris as I was working there at the
[LNHB](www.lnhb.fr/en), the national radionuclide metrology laboratory. I had
one week free in August and I decided that it is time to do something
interesting/stupid, so I bought some train tickets and AirBnB reservations and
there I was: waiting my train to Grenoble at 8 o'clock in the morning at Gare
de Bercy.

I was very early, as always, because I have a little fear of missing trains or
planes. This is not a problem, but, as it turned out, it is good to be early for
French trains. There are limited spaces for bikes and the logic is first come -
first serve... until there are no more places. Well, I guess I waited a bit too
much before entering the train and there were no mo spaces left. The **chef de
train** was good enough to help me squeeze my bike in there somewhere, but
beware! It's better to be early.

After a few minutes the train was rolling and everything was great.
Unfortunately for me, my seat was quite far from my bike and I was a little
worried that some harm may come to it. After a while, though, the train
stopped... indefinitely... and I had all the time I wanted to stay near the
bike. There was some problem on some station ahead and we had to wait for three
whole hours before the train started again. That messed my plans up a bit,
because I had to ride more than 30 kilometers with around 900 meters climb to
get to my description for the night.

The train arrived finally at Grenoble at 5:30 PM, after a 3 h delay.
Unfortunately I could not take a look around Grenoble and I had to be on my way
immediately to make up for the lost time.

![The 32 km that I had to pass on Day One.](/images/grenoble/Screenshot_20190810-163137_OsmAnd-01.jpeg){:width="75%" .center-image}

In the end I managed not to get lost more than usual on the way to the AirBnB.
The first 20 km of the way were along the river and were really amazing. Nice
warm weather, chill river breeze, smooth gravel road and then I got to the
incline. A steep climb of 900 meters for the next 10 km awaited me. The sun was
already going down and my AirBnB host got a bit worried for me. He actually
suggested to pick me up with his car, but I kindly refused. I was not going to
give up on my first day. The sun was setting over the steep cliffs on the
adjacent slopes to the west and the sky was painted in dark orange. A beautiful
setting to distract me from the incoming fatigue.

![The sun setting over Grenoble](/images/grenoble/IMG_20190810_194926.jpg){:width="75%" .center-image}

After a long struggle with the hill I managed to get in time for dinner at my
AirBnB at 8:30 PM. In the end I managed to make the 32 km trip for 2 h and 30
minutes. Not so bad for a first day.

I arrived at my destination and the AirBnB host, German from Argentina, invited
me with him and his girlfriend for a dinner. We talked a bit and it turned out
that he is working as a support at RedHat. A Free Software guy living in the
mountain! How cool is that! He had made many small home automation devices which
were really interesting, for example: a meteo station and an automatic door for
his cat Getiti. The cat seemed interested in my bike and wanted to go with me,
but after he realized that all my food consists of two sandwiches he bailed out.
Typical cat stuff.

![Getiti wondering what would anybody want from the outside world, where the
food doesn't come automagically.](/images/grenoble/IMG_20190811_073539.jpg){:width="50%" .center-image}

## Day Two

I woke up at around seven and a half and had a quick breakfast after which I
said goodbye to the nice AirBnB host and his cat and I started my trip to the
next destination. It was going to be a long ride today. Seventy kilometers
starting with a 10 km steep descent over a road followed by thirty kilometers of
gravel and a final 30 km ascent along mountain roads.

![The road for Day Two.](/images/grenoble/Screenshot_20190812-083905_OsmAnd.png){:width="50%" .center-image}

In the beginning I had a nice long descent to the valley. The winding road took
me around 30 minutes and I arrived at the start of my long journey to the next
AirBnB. The path though the valley was truly magnificent, as most of it was
a gravel road along the rail track with amazing views in the distance and all
around. After about 15-20 km the my path and the railroad split up and I found
myself going through someones beautiful vineyards and you could see Mont Blanc
guarding them from distance. Somewhere there I stopped for a well deserved rest
and a meal which consisted of a two day old sandwich. I know, it doesn't sound
so nice, but hey, you gotta make sacrifices if you want to experience something
like this.

![View towards Mont Blanc](/images/grenoble/IMG_20190811_105535.jpg){:width="75%" .center-image}

I couldn't get much rest as it was just around noon and the sun was scorching
everything that was not shaded. I had to choose between waiting to find a shade
and eating then and there. I chose the latter, because what awaited next was a
1000 meter ascent along a quite steep and blazing hot asphalt road. In fact, the
road was so hot that I had to stop every 20-30 minutes to cool myself under some
tree. This was one of the most difficult sections of my whole trip. 10/10 would
do again.

![Resting under a tree, trying to forget about the climb](/images/grenoble/IMG_20190811_120944.jpg){:width="50%" .center-image}

After some struggle I managed to get to the highest point for the day and from
there it was an easy downward ride up to the next stop. I managed to stop along
the way for a cold beer and some snacks. What a pleasant way to finish the day.

![Highest point of Day Two.](/images/grenoble/IMG_20190811_142232.jpg){:width="75%" .center-image}

I reached the AirBnB and it turned out to be a very nice two story house just
near the main road. Fortunately, not too many cars pass by it, so it was quiet.
In fact, the place was so nice, that I visited again next year, but this is
a story for another time. The hosts are very pleasant people and when they heard
that I had such a long trip they hurried to make some dinner which was very kind
of them. We had grilled sausages and potatoes and we spoke the whole evening in
broken French and English. After dinner I went early to bed to prepare myself
for...

## Day Three

As the forecast accurately predicted, the morning was cold, dark and rainy, but
that didn't bother me that much, because the chances were that the weather would
become better later during the day. I decided to go by foot to the highest peak
I could see near me: Mont Colombier. It looks grandiose and is not too far away.

![Mont Colombier](/images/grenoble/IMG_20190812_185813.jpg){:width="75%" .center-image}

The start of the climb to the peak was around 10 kilometers from the place I was
staying. To get to it I had to walk down the main road which was not exactly
pleasant but nevertheless I managed to reach the place in around two hours. Just
before the start of my ascent it started raining heavily, but fortunately there
was a small bus stop that gave me shelter for around 40 minutes while I was
waiting for the rain to stop. I went up hastily in hopes that the rain will not
catch me unprepared.

![The path to Mont Colombier. The start is just near the Aillon-le-Vieux village.](/images/grenoble/Screenshot_20190812-213405_OsmAnd.png){:width="75%" .center-image}

The start of the climb started at around 850 meters above sea level and the
height of the peak is 2050 meters. The slope on the way up was quite challenging
and reached 45 degrees at some points. The rain had soaked the soil and
everything was extremely slippery, so for every two steps forward you went a
step backwards. It took me around two hours to reach the peak and there I saw a
big wooden cross covered in a thick fog.

![A seashell at 2050 m above sea level.](/images/grenoble/IMG_20190812_150019.jpg){:width="75%" .center-image}

When I went to the top it was already 3 PM and I hurried to get back as the
weather was far from perfect for long walks and I was all muddy and wet. I
constantly slid down as if I was walking on freshly frozen ice. At one point I
took a bad step and twisted my leg. The 10 km of limping back to the AirBnB were
not pleasant at all. Despite all of the problems, the climb was very nice I
would still like to do it in a better weather.

## Day Four

With waking up I felt that the day is going to be hard. I had a lot of sore
muscles and my right leg was hurting a little. I decided to at least try to do
some sightseeing because the weather was better that day and I headed out to the
Col de l'Arclusaz. The path was 18 km along something that was supposed to be a
nice path but turned out to be quite steep. I was with the bike but most of the
time I was pushing it to go up.

![The track of the fourth day.](/images/grenoble/Screenshot_20190814-210907_OsmAnd.png){:width="50%" .center-image}

I managed to get almost to the top but he fog came back and I decided to
withdraw. Besides, my leg was hurting and it started to get quite cold. The best
was to go back and rest for the journey that was to come tomorrow. The descent
was pleasant, albeit a bit short.

## Day Five

On the last day I was to go to Geneva. The morning welcomed me with a thick fog.
The sky was faintly visible through the fog which meant that it is no too high
and I can probably go above it. The journey starts with around a 300 meters
ascent. I packed my bags and off I went. Geneva here I come.

![The track for Day Five.](/images/grenoble/Screenshot_20190814-211428_OsmAnd.png){:width="50%" .center-image}

The first stop on the way was the beautiful city of Annecy located near a
picturesque lake. I was a bit ahead of schedule so I decided to stay on the beach
near the lake and rest for a while. I ate some canned tuna and bread and resumed
my trek. I was not sure how the journey was about to continue as Osmand manages
to surprise me every time. And this was no exception.

![Annecy lake](/images/grenoble/IMG_20190814_083824.jpg){:width="75%" .center-image}

Somewhere in the middle of the road I found out that I was riding on one side of
a very large canyon which I would guess was at least 150 meters deep and 200
meters wide. Osmand was here to save the day and I passed this very
interestingly looking bridge which seems like a local tourist site. I am glad I
managed to see it completely by accident.

![Some interesting bridge between Annecy and Geneva](/images/grenoble/IMG_20190814_105417.jpg){:width="75%" .center-image}

Right after the bridge I had to clear around 500 meters of ascent. It was
already noon and it became increasingly warmer which made the cycling a bit
challenging. The pain in my right foot was not helping also and I was forced to
push the bike for the most part on this section. I became a bit scared that I
would not be able to finish my ride in time and the train does not wait for
anyone.

![The famous Jet d'Eau.](/images/grenoble/IMG_20190814_185739.jpg){:width="75%" .center-image}

I had my second Osmand surprise for the day when I found out that most of my way
to Geneva is on the newly built highway. Imagine the looks of all the
construction workers when they saw a guy on a bike passing by the large trucks,
cranes and bulldozers. I am wondering what would have happened if the highway
was already built and not still under construction...

Despite that I was in Geneva at 1:30 PM, around 30 minutes earlier than
anticipated. The Geneva lake and the famous Jet d'Eau greeted me when I arrived
to the beautiful city.

![End point of the bike path.](/images/grenoble/IMG_20190814_135229.jpg){:width="75%" .center-image}

I took one last look at the Mont Blanc and I took my train Zürich.

[gpx track from Day One](/files/route.gpx)

{% include mathjax.js %}
