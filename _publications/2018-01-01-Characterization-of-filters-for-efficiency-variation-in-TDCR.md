---
title: "Characterization of filters for efficiency variation in TDCR"
collection: publications
section: metrology
date: 2018-01-01
venue: "In the proceedings of 2018 IEEE Nuclear Science Symposium and Medical Imaging Conference Proceedings (NSS/MIC)"
authors: <b>Ch. Dutsov</b>,  K. Mitev,  Ph. Cassette
---

Use [Google Scholar](https://scholar.google.com/scholar?q=Characterization+of+filters+for+efficiency+variation+in+TDCR){:target="\_blank"} for full citation
