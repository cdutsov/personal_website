---
title: "Study of $^{222}$Rn Absorption and Detection Properties of EJ-212 and BC-400 Plastic Scintillators"
collection: publications
section: radon
date: 2017-01-01
venue: "IEEE Trans. Nucl. Sci."
authors: K. Mitev, <b>Ch. Dutsov</b>, S. Georgiev, L. Tsankov, T. Boshkova
---

Use [Google Scholar](https://scholar.google.com/scholar?q=Study+of+Rn+222+Absorption+and+Detection+Properties+of+EJ+212+and+BC+400+Plastic+Scintillators){:target="\_blank"} for full citation
