---
title: "Time-domain based evaluation of detection efficiency in liquid scintillation counting"
collection: publications
section: metrology
date: 2021-06-02
venue: Scientific Reports
authors: K. Mitev, <b>Ch. Dutsov</b>, Ph. Cassette, B. Sabot
link: https://www.nature.com/articles/s41598-021-91873-1
---

This work explores the distribution of time intervals between signals from the
photomultiplier tubes (PMTs) of a liquid scintillation counting (LSC) system
when a scintillation burst caused by an ionizing particle is detected. This
distribution is termed the cross-correlation distribution and it is shown that
it contains information about the probability to detect a scintillation event. A
theoretical model that describes the cross-correlation distribution is derived.
The model can be used to estimate the mean number of detected photons in a LSC
measurement, which allows the calculation of the detection efficiency. The
theoretical findings are validated by Monte Carlo simulations and by experiments
with dedicated LSC systems and several commercial LSC cocktails. The results
show that some of the parameters of the cross-correlation distribution such as
the peak height or the kurtosis can be used as detection efficiency estimators
or quenching indicators in LSC. Thus, although the time domain and the
cross-correlation distribution have received little to no attention in the
practice of LSC, they have the capacity to bring significant improvements in
almost all LSC applications related to activity determination. The results also
suggest concepts for
