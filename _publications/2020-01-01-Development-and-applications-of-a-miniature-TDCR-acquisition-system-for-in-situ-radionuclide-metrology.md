---
title: "Development and applications of a miniature TDCR acquisition system for in-situ radionuclide metrology"
collection: publications
date: 2020-01-01
venue: Nucl. Instrum. Meth. A
section: metrology
authors: V. Jordanov, Ph. Cassette, <b>Ch. Dutsov</b>, K. Mitev
link: https://doi.org/10.1016/j.nima.2018.09.037
---
