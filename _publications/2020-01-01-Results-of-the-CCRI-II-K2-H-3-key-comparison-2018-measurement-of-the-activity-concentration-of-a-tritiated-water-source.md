---
title: "Results of the CCRI (II)-K2. H-3 key comparison 2018: measurement of the activity concentration of a tritiated-water source"
collection: publications
date: 2020-01-01
venue: "Metrologia"
section: hidden
authors: Ph. Cassette, A. Arinc, M. Capogni,P. De Felice, <b>Ch. Dutsov</b>, R. Galea et al.
---
