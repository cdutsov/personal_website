---
title: "Measurement of the half-life of excited nuclear states using liquid scintillation counting"
collection: publications
section: metrology
date: 2021-07-01
venue: Appl. Radiat. Isot.
authors: <b>Ch. Dutsov</b>, B. Sabot, Ph. Cassette, K. Mitev
link: https://physica.dev/files/preprints/Dutsov_et_al_2021_half_lives_LSC.pdf
---

This work presents measurements of the half-lives of excited nuclear
states of <sup>237</sup>Np and <sup>57</sup>Fe using a liquid scintillation
spectrometer and a gamma detector. A novel approach for the
determination of the half-lives of some excited states is presented
which uses only LS counting data from a detector with two PMTs.

The lifetime of
the 1^st^ and 2^nd^ excited states of
<sup>57</sup>Fe were obtained without the use of a gamma detector. The obtained
value for the 59.54 keV level of <sup>237</sup>Np is 67.60(25) ns. The obtained
values for the 14.4 keV and 136.5 keV levels of <sup>57</sup>Fe are 97.90(40) ns
and 8.780(36) ns, respectively. The half-life
results from this study are consistent with the average value found in
the reference decay data tables and have a lower uncertainty.

[Science Direct link](https://authors.elsevier.com/a/1dLOB3SWWg3XFV)
