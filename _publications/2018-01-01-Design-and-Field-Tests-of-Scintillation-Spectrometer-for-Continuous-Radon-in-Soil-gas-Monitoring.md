---
title: "Design and Field Tests of Scintillation Spectrometer for Continuous Radon in Soil-gas Monitoring"
collection: publications
section: radon
date: 2018-01-01
venue: "In the proceedings of 2018 IEEE Nuclear Science Symposium and Medical Imaging Conference Proceedings (NSS/MIC)"
citation: " Krasimir Mitev,  Chavdar Dutsov,  Ludmil Tsankov,  Strahil Georgiev,  Mityo Mitev,  Nikolay Markov,  Todor Todorov, &quot;Design and Field Tests of Scintillation Spectrometer for Continuous Radon in Soil-gas Monitoring.&quot; In the proceedings of 2018 IEEE Nuclear Science Symposium and Medical Imaging Conference Proceedings (NSS/MIC), 2018."
---

Use [Google Scholar](https://scholar.google.com/scholar?q=Design+and+Field+Tests+of+Scintillation+Spectrometer+for+Continuous+Radon+in+Soil+gas+Monitoring){:target="\_blank"} for full citation
