---
title: "Study of two different coincidence counting algorithms in TDCR measurements"
collection: publications
section: metrology
date: 2019-01-01
venue: "Appl. Radiat. Isot."
link: 10.1016/j.apradiso.2019.108895
authors: <b>Ch. Dutsov</b>, K. Mitev, Ph. Cassette, V. Jordanov
---

This work presents a comparison of two different logics for imposing
extending-type dead-time in TDCR measurements: the common dead-time (CDT) and
the individual dead-time (IDT) counting logics.

The CDT is implemented in the widely used MAC3 TDCR counting module and the IDT
was recently implemented in the nanoTDCR counting device. The performance of the
two counting algorithms is evaluated by three experimental setups and a
dedicated Monte Carlo (MC) code for the simulation of realistic TDCR events. An
excellent agreement is observed between the two counting logics for measurements
of the pure β-emitting radionuclides ³H, ¹⁴C, ⁶³Ni and ⁹⁰Sr/⁹⁰Y for which the
relative deviations in measured activities are in all cases less than 0.27%. For
the measured ²²²Rn sources, we observe relative deviations up to 0.25% between
the logical sum of double coincidences counting rates obtained with the two
counting logics. The differences are in the double coincidence counting rate
estimates which propagate to the estimates of the activity. Excellent agreement
was observed between the CDT and IDT in MC simulated measurements of ³H where
relative deviations are less than 0.24% for activities up to 70 kBq. The IDT
counting algorithm seems to have an advantage in this particular case as it
results in the same counting rates and calculated activities but with a
significant reduction in the double coincidences dead-time.
