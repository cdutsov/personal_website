---
title: "In quest of the optimal coincidence resolving time in TDCR LSC"
collection: publications
section: metrology
date: 2021-01-21
venue: Nucl. Instrum. Meth. A
authors: <b>Ch. Dutsov</b>, Ph. Cassette, K. Mitev, B. Sabot
link: https://doi.org/10.1016/j.nima.2020.164846
---

This paper presents studies of the influence of the coincidence resolving time
on the activity calculated by the Triple-to-Double Coincidences Ratio (TDCR)
method in Liquid Scintillation (LS) counting. Recently, published methods for
the correction for accidental coincidences in TDCR counting open the possibility
to use resolving times up to several microseconds, long enough not to miss true
coincidences and to study the effects of delayed fluorescence.

H-3, C-14, Fe-55 and Ni-63 LS-sources in UltimaGold (UG), UG LLT and Toluene+PPO
cocktails were measured using a TDCR counter connected to a digitizer working in
list-mode. The necessary resolving time to include 99.9% of the logical sum of
double (D) coincidences was found to be 1.2 microseconds for H-3, 1 microseconds
for Fe-55 and 500 ns for Ni-63 in UG. The activity of all LS-sources was
calculated using the TDCR method for resolving times from 10 ns to 2
microseconds and a significant dependence between the calculated activity
and resolving time was observed. A dedicated Monte Carlo (MC) code was used
to simulate list-mode data from TDCR measurements. The simulation results
suggest that the H-3 activity calculated by the TDCR method is overestimated
regardless of the used resolving time if delayed fluorescence is present
which is not described by the used ionization quenching function.

Efficiency variation measurements of H-3 in UG LLT show a strong dependence of
the optimal $kB$ parameter on the used resolving time: 85 um/MeV at 40 ns and
110 um/MeV at 200 ns, leading to 2.5% difference in calculated activity. In the
framework of this study the efficiency variation methods by chemical quenching
and gray filters were compared and a difference of 60 um/MeV between the two was
observed.

The results from this article demonstrate that regardless of the
available corrections for accidental coincidences, it is not advisable to
increase the resolving time beyond what is necessary to register all prompt
fluorescence events. Moreover, even for short coincidence resolving times,
delayed fluorescence could have a significant influence on the activities
calculated by the TDCR method.
