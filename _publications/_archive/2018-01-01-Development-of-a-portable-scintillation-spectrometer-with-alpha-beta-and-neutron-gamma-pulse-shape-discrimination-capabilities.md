---
title: "Development of a portable scintillation spectrometer with alpha-/beta-and neutron-/gamma-pulse-shape discrimination capabilities"
collection: publications
permalink: /publication/2018-01-01-Development-of-a-portable-scintillation-spectrometer-with-alpha-beta-and-neutron-gamma-pulse-shape-discrimination-capabilities
date: 2018-01-01
venue: 'In the proceedings of 2018 IEEE Nuclear Science Symposium and Medical Imaging Conference Proceedings (NSS/MIC)'
citation: ' K Mitev,  V Jordanov,  M Hamel,  Ch Dutsov,  S Georgiev,  P Cassette, &quot;Development of a portable scintillation spectrometer with alpha-/beta-and neutron-/gamma-pulse-shape discrimination capabilities.&quot; In the proceedings of 2018 IEEE Nuclear Science Symposium and Medical Imaging Conference Proceedings (NSS/MIC), 2018.'
---
Use [Google Scholar](https://scholar.google.com/scholar?q=Development+of+a+portable+scintillation+spectrometer+with+alpha+/beta+and+neutron+/gamma+pulse+shape+discrimination+capabilities){:target="_blank"} for full citation