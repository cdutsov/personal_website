---
title: "Methods for the experimental study of $^{220}$Rn homogeneity in calibration chambers"
collection: publications
permalink: /publication/2020-01-01-Methods-for-the-experimental-study-of-Rn-220-homogeneity-in-calibration-chambers
date: 2020-01-01
venue: "Appl. Radiat. Isot."
---

Use [Google Scholar](https://scholar.google.com/scholar?q=Methods+for+the+experimental+study+of+Rn+220+homogeneity+in+calibration+chambers){:target="\_blank"} for full citation
