---
title: "Electronic circuits for the high voltage supply and additional sensors for the polyphemus Rn-222 in soil-gas scintillation detector"
collection: publications
permalink: /publication/2017-01-01-Electronic-circuits-for-the-high-voltage-supply-and-additional-sensors-for-the-polyphemus-Rn-222-in-soil-gas-scintillation-detector
date: 2017-01-01
venue: 'In the proceedings of 2017 XXVI International Scientific Conference Electronics (ET)'
citation: ' Chavdar Dutsov,  Mityo Mitev,  Ludmil Tsankov,  Krasimir Mitev, &quot;Electronic circuits for the high voltage supply and additional sensors for the polyphemus Rn-222 in soil-gas scintillation detector.&quot; In the proceedings of 2017 XXVI International Scientific Conference Electronics (ET), 2017.'
---
Use [Google Scholar](https://scholar.google.com/scholar?q=Electronic+circuits+for+the+high+voltage+supply+and+additional+sensors+for+the+polyphemus+Rn+222+in+soil+gas+scintillation+detector){:target="_blank"} for full citation