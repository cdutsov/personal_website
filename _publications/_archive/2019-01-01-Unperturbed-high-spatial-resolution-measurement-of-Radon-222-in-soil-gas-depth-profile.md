---
title: "Unperturbed, high spatial resolution measurement of Radon-222 in soil-gas depth profile"
collection: publications
permalink: /publication/2019-01-01-Unperturbed-high-spatial-resolution-measurement-of-Radon-222-in-soil-gas-depth-profile
date: 2019-01-01
venue: 'Journal of environmental radioactivity'
citation: ' K Mitev,  Ch Dutsov,  S Georgiev,  T Boshkova,  D Pressyanov, &quot;Unperturbed, high spatial resolution measurement of Radon-222 in soil-gas depth profile.&quot; Journal of environmental radioactivity, 2019.'
---
Use [Google Scholar](https://scholar.google.com/scholar?q=Unperturbed,+high+spatial+resolution+measurement+of+Radon+222+in+soil+gas+depth+profile){:target="_blank"} for full citation