---
title: "Partition Coefficients and Diffusion Lengths of Rn-222 in Some Polymers at Different Temperatures"
collection: publications
permalink: /publication/2019-01-01-Partition-Coefficients-and-Diffusion-Lengths-of-Rn-222-in-Some-Polymers-at-Different-Temperatures
date: 2019-01-01
venue: 'International journal of environmental research and public health'
citation: ' Strahil Georgiev,  Krasimir Mitev,  Chavdar Dutsov,  Tatiana Boshkova,  Ivelina Dimitrova, &quot;Partition Coefficients and Diffusion Lengths of Rn-222 in Some Polymers at Different Temperatures.&quot; International journal of environmental research and public health, 2019.'
---
Use [Google Scholar](https://scholar.google.com/scholar?q=Partition+Coefficients+and+Diffusion+Lengths+of+Rn+222+in+Some+Polymers+at+Different+Temperatures){:target="_blank"} for full citation