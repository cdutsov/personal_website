---
title: "Design and Tests of a Detector for $^{222}$Rn in Soil-gas Measurements based on $^{222}$Rn Absorbing Scintillating Polymers"
collection: publications
section: radon
date: 2017-01-01
venue: "In the proceedings of 2017 IEEE Nuclear Science Symposium and Medical Imaging Conference (NSS/MIC)"
---

Use [Google Scholar](https://scholar.google.com/scholar?q=Design+and+Tests+of+a+Detector+for+Rn+222+in+Soil+gas+Measurements+based+on+Rn+222+Absorbing+Scintillating+Polymers){:target="\_blank"} for full citation
