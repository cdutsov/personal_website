---
title: "Evaluation of the accidental coincidence counting rates in TDCR counting"
collection: publications
date: 2020-10-11
venue: Nucl. Instrum. Meth. A
section: metrology
authors: <b>Ch. Dutsov</b>, Ph. Cassette, K. Mitev, B. Sabot
link: https://doi.org/10.1016/j.nima.2020.164292
---

This paper presents analytical and experimental methods to evaluate the
accidental coincidence counting rates in a Triple-to-Double Coincidence Ratio
(TDCR) Liquid Scintillation (LS) measurement.

The experimental method we propose is based on the analysis of the distribution
of the time delays between the first detected events in each photomultiplier
tube. The underlying assumption is that events separated by several microseconds
in time are not correlated, thus the accidental coincidence counting rates could
be determined from the time interval distribution of uncorrelated events. The
analytical evaluation of the accidental coincidence counting rates is based on
the conditional probabilities for the occurrence of uncorrelated events within
the same coincidence resolving time. The analytical and experimental evaluations
of the rate of accidental coincidences give consistent results for TDCR
measurements of $^{3}$H, $^{55}$Fe and $^{14}$C. The two methods were used to
evaluate corrections for accidental coincidences for Monte Carlo (MC) generated
list-mode files of $^{3}$H measurements with increasing activities. The counting
rates, corrected for accidental coincidences using the analytical method, are
within 0.29% of the MC reference up to 100 kBq and the corrected, using the
experimental method, are within 0.21% up to 200 kBq.

{% include mathjax.js %}
