---
title: "Design and performance of a miniature TDCR counting system"
collection: publications
section: "metrology"
date: 2017-01-01
venue: "J Radioanal. Nucl. Chem."
authors: K. Mitev, Ph. Cassette, V. Jordanov, Liu, HR, <b>Ch. Dutsov</b>
---

Use [Google Scholar](https://scholar.google.com/scholar?q=Design+and+performance+of+a+miniature+TDCR+counting+system){:target="\_blank"} for full citation
