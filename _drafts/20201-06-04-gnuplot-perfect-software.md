---
title: "Thoughts on perfect software and gnuplot"
date: 2021-06-05
permalink: /posts/2021/06/thoughts-on-perfect-software-and-gnuplot.html
tags:
  - gnuplot
  - opinions
---


![Fancy drop cap at the begining of the chapter](/images/posts/fancy-lettrine.png){:width="90%" .center-image}
